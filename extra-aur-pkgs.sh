#!/usr/bin/env bash
#-------------------------------------------------------------------------
echo -ne "


██╗  ██╗ ██████╗ ████████╗████████╗███████╗███████╗██╗    ██╗ █████╗ ██████╗  █████╗ ███╗   ██╗
██║ ██╔╝██╔═══██╗╚══██╔══╝╚══██╔══╝██╔════╝██╔════╝██║    ██║██╔══██╗██╔══██╗██╔══██╗████╗  ██║
█████╔╝ ██║   ██║   ██║      ██║   █████╗  ███████╗██║ █╗ ██║███████║██████╔╝███████║██╔██╗ ██║
██╔═██╗ ██║   ██║   ██║      ██║   ██╔══╝  ╚════██║██║███╗██║██╔══██║██╔══██╗██╔══██║██║╚██╗██║
██║  ██╗╚██████╔╝   ██║      ██║   ███████╗███████║╚███╔███╔╝██║  ██║██║  ██║██║  ██║██║ ╚████║
╚═╝  ╚═╝ ╚═════╝    ╚═╝      ╚═╝   ╚══════╝╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝

#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------
"
echo
echo "INSTALLING AUR SOFTWARE"
echo

cd "${HOME}"

echo "CLOING: PARU"
git clone "https://aur.archlinux.org/paru.git"

PKGS=(
'autojump'
'brave-bin'
'bridge-utils'
'dxvk-bin'
'lightly-fork-git'
'lightlyshaders-git'
'mangohud'
'mangohud-common'
'nerd-fonts-fira-code'
'nordic-darker-standard-buttons-theme'
'nordic-darker-theme'
'nordic-kde-git'
'nordic-theme'
'ocs-url'
'plymouth-git'
'sddm-nordic-theme-git'
'snapper-gui-git'
'ttf-meslo'
'vde2'
'wps-office-bin'
'ttf-times-new-roman'
'ventoy-bin'
'kwin-scripts-forceblur'
'vimix-cursors'
'kwin-scripts-window-colors'
'libtiff5'
'preload'
'linux-xanmod-linux-bin-x64v3'
'anydesk'
'hunspell-en-med-glut-git'
'times-newer-roman'
'ttf-wps-fonts'
'stacer-bin'
'whitesur-kde-theme-git'
'whatsdesk-bin'
'noisetorch'
'Aspell-ta'
'bamini-tamil-font'
'aic94xx-firmware'
'ast-firmware'
'wd719x-firmware'
'upd72020x-fw'
'mkinitcpio-firmware'
'auto-cpufreq'
'ttf-ms-fonts'
'ttf-wps-win10'
'wps-office-all-dicts-win-languages'
)

cd ${HOME}/paru
makepkg -si

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    paru -S "$PKG" --noconfirm --needed
done


echo
echo "Done!"
echo














