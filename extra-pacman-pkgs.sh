#!/usr/bin/env bash
#-------------------------------------------------------------------------
echo -ne "


██╗  ██╗ ██████╗ ████████╗████████╗███████╗███████╗██╗    ██╗ █████╗ ██████╗  █████╗ ███╗   ██╗
██║ ██╔╝██╔═══██╗╚══██╔══╝╚══██╔══╝██╔════╝██╔════╝██║    ██║██╔══██╗██╔══██╗██╔══██╗████╗  ██║
█████╔╝ ██║   ██║   ██║      ██║   █████╗  ███████╗██║ █╗ ██║███████║██████╔╝███████║██╔██╗ ██║
██╔═██╗ ██║   ██║   ██║      ██║   ██╔══╝  ╚════██║██║███╗██║██╔══██║██╔══██╗██╔══██║██║╚██╗██║
██║  ██╗╚██████╔╝   ██║      ██║   ███████╗███████║╚███╔███╔╝██║  ██║██║  ██║██║  ██║██║ ╚████║
╚═╝  ╚═╝ ╚═════╝    ╚═╝      ╚═╝   ╚══════╝╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝

#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------
"
echo
echo "INSTALLING SOFTWARE"
echo

PKGS=(
'onboard'
'telegram-desktop'
'gufw'
'exfat-utils'
'gnome-disk-utility'
'kdeconnect'
'firefox'
'nemo'
'libqalculate'
'qalculate-gtk'
'hunspell'
'hunspell-en_us'
'vlc'
'pdfarranger'
'lib32-pipewire'
'gnome-keyring'
'packagekit-qt5'
'intel-media-driver'
'intel-media-sdk'
'BC'
'exfatprogs'
'gsmartcontrol'
'linux-firmware-qlogic'
'papirus-icon-theme'
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

echo
echo "Done!"
echo
